/* Author:http://www.rainatspace.com

*/

function initializeScript(){
	jQuery('ul li#laptop-products').on('mouseover',function(e){
		e.stopPropagation();
		jQuery('ul li#laptop-products').addClass('show-dropdown');
		jQuery('.dropdown-custom').fadeIn('slow');
	});
	jQuery('.dropdown-custom').hover(function(){
		jQuery('ul li#laptop-products').addClass('show-dropdown');
		jQuery(this).fadeIn('slow')
	},function(){
		jQuery('ul li#laptop-products').removeClass('show-dropdown');
		jQuery(this).fadeOut('slow')
	});
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();

    //RESPONSIVE NAV
	jQuery('#menu').slicknav();
	jQuery('#dropdown-toggle').dropdown();


});
/* END ------------------------------------------------------- */